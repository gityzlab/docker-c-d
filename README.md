# docker-c-d

#### Feature:

Caddy2 as frontend http server with [Diffuse](https://github.com/icidasset/diffuse) as disquise.

Thanks to lxhao61 for rendering ["Caddy2 with trojan-go plugin"](https://github.com/lxhao61/integrated-examples/releases)

### Deploy:

Deploy Caddy-trojan-go by docker.

Just fork this repo and deploy.

# Environment

You need to add Secrets(System environment variables) like:

### key: "uuid"

### value: "your own uuid"

The secrets is private so you don't need to worry about leaking your data.

# Clients

You can find all clients from:
[**this page**](https://itlanyan.com/trojan-go-clients-download/)

# Usage

- type:"trojan-go"
- password:"uuid"
- host:"your website url"
- port:443
- stream-network:ws
- ws-path:/
- tls:tls

# Sleep

May the service sleep,you need a web monitor like UptimeRobot to keep it work.
https://uptimerobot.com



# Best wishes!
